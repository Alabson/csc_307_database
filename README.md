# CSC 307 DATABASE ASSIGNMENT #



## Solution ##

### Step 1: Set Up mysqldb ###

* Run xampp control panel
* start mysql db
* click shell to start terminal
* start as user eg root


``` sql
	mysql -u root
	show databases; // to list available databases
	create database csc307_assignment; // to create database
	use csc307_assignment; // to select db
```




### Step 2: Create Tables and Attributes ###

* Creating tables

``` sql
	create table suppliers(
	   sid INT NOT NULL AUTO_INCREMENT,
	   sname VARCHAR(50) NOT NULL,
	   address VARCHAR(100) NOT NULL,
	   PRIMARY KEY ( sid )
	);
```

``` sql
	create table parts(
	   pid INT NOT NULL AUTO_INCREMENT,
	   pname VARCHAR(50) NOT NULL,
	   color VARCHAR(100) NOT NULL,
	   PRIMARY KEY ( pid )
	);
```

``` sql
	create table catalog(
	   sid INT NOT NULL,
	   pid INT NOT NULL,
	   cost real NOT NULL,
	);
```

>List tables using
`show tables;`

### Create Mock User Data ###

* Insert Query
``` sql
	INSERT INTO suppliers
	->(sname, address)
	->VALUES
	->("John Snow", "No, 222 Rwang Pam Street");
```
>No need to insert for sid cause it's auto increment and also the primary key

* Insert Query for `parts` Table
``` sql
	INSERT INTO parts
	->(pname, color)
	->VALUES
	->("Laptop Skin", "Green");
```

* Insert Query for `catalog` Table
``` sql
	INSERT INTO catalog
	->(sid, pid, cost)
	->VALUES
	->(2, 1, 32);
```

### To Select The `sid` of Suppliers that resides at Rwang Pam Street ###

``` sql
	SELECT sid FROM suppliers WHERE address="No. 222 Rwang Pam Street";
```

### Essence of Database Normalization ###

The Relational Model of data management was first developed by Dr. Edgar F. Codd in 1969. Modern relational database management systems (RDBMSes) are aligned with the paradigm. The key structure identified with RDBMS is the logical structure called a “table”. Tables are primarily composed of rows and columns (also called records and attributes or tuples and fields). In a strict mathematical sense, the term table is actually referred to as a relation and accounts for the term “Relational Model”. In mathematics, a relation is a representation of a set.

The expression attribute gives a good description of the purpose of a column – it characterizes the set of rows associated with it. Each column must be of a particular data type and each row must have some unique identifying characteristics called “keys”. Data change is typically more efficient when done using the relational model while data retrieval may be faster with the older Hierarchical Model which has been redefined in model NoSQL systems.

Data Normalization is a mathematical process of modeling business data into a form which ensures that each entity is represented by a single relation (table). The early proponents of the relational model proposed a concept of Normal Forms. Edgar Codd defined the first, the second, and third Normal Forms. He was then joined by Raymond F. Boyce. Together they defined the Boyce-Codd Normal Form. By now, six Normal Forms are defined theoretically but in most practical applications, we typically extend Normalization up to the Third Normal Form. Each Normal Form strives to avoid anomalies during data modification, reduce the redundancy and dependency of data within a table. Each level of normalization tends to introduce more tables, reduce redundancy, increase the simplicity of each table but also increases the complexity of the entire relational database management system. So structurally RDBM systems tend to be more complex than Hierarchical systems.
###  Why Normalization: Four Anomalies  ###

Data storage without normalization causes a number of problems with data consumption. The proponents of normalization called such problems anomalies. In order to describe these anomalies, let’s look at the data presented in Fig. 1.

This table represents in essence two sets of data that have been inadvertently combined: staff names and departments. Notice that all staff is from the same department: Engineering. That was done for simplicity and in order to demonstrate normalization. There are three main problems associated with manipulating this structure:

The Insertion Anomaly

In order to insert a new record, we have to keep repeating the department and manager names.

The Deletion Anomaly

In order to delete a staffer’s record, we must also delete the associated manager and department. If there is a need to remove ALL staffers’ records, we must also remove all departments and all managers.

The Update Anomaly

If there is a need to change the manager of any department, we must make the change in every single row of this table since the values are duplicated for each staffer.

Normal Forms
In the following sections of the article, we shall try to describe the 1st, the 2nd, and the 3rd Normal Forms which are much more likely to be observed in real RDBM Systems. There are other extensions of the theory such as the Fourth, the Fifth, and Boyce-Codd Normal Forms but in this article, we shall limit ourselves to Three Normal Forms.

The First Normal Form

The 1st Normal Form is defined by four rules:

Each column must contain values of the same data type.

The Staffers table already meets this rule.

Each column in a table must be atomic.

This essentially means you should divide the contents of a column until they can no longer be divided. Notice that the Role column in the Staffers table breaks rule 2 for the row with StaffID=3.

Each row in a table must be unique.

Uniqueness in normalized tables is typically achieved using Primary Keys. A Primary Key uniquely defines each row in a table. Most of the time a Primary Key is defined by only one column. A Primary Key composed of more than one column is called a Composite Key.

The order in which records are stored does not matter.

The Second Normal Form

The 1st Normal form must already be in place.

Every non-key column must not have Partial Dependency on the Primary Key.

The thrust of the second rule is that all columns of the table must depend on all columns that comprise the Primary Key together. Looking back at the tables in Figures 2, 3 and 4, we find we have achieved all the requirements of the First Normal Form. We have also achieved the requirements of the Second Normal Form for two tables Roles and Departments. However, in the case of the Staffers table, we still have a problem. Our Primary Key is composed of the columns StaffID and RoleID.

Rule 2 of the Second Normal Form is broken here by the fact that the Gender and DateofBirth of staff do not depend on the RoleID. There is a Partial Dependency.

The Third Normal Form

The 2nd Normal form must already be in place.

Every non-key column must not have Transitive Dependency on the Primary Key.

The thrust of the third normal form is that there mustn’t be any columns that depend on non-key columns even if that non-key columns already depend on the Primary Key.

As an example, assume we decided to add an additional column to the Staffers table as shown in Fig. 7 in order to clearly see the staffer’s manager. By doing that we would have broken the second Third Normal Form rule, because the Manager Name depends on the DeptID and the DeptID, in turn, depends on the StaffID. This is a Transitive Dependency.

### Conclusion ###

Data Normalization lies in the realm of Database Design and both developers and DBAs should pay attention to the rules outlined in this article. It is always better to get normalization done before the database goes into production. The benefits of a properly designed Relational Database Management System are simply worth the effort.